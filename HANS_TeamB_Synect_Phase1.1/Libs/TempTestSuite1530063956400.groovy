import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/TS_Phase1')

suiteProperties.put('name', 'TS_Phase1')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())



RunConfiguration.setExecutionSettingFile("/Users/hansnicolaus/Documents/Synect_Interns_Phase1/Phase1/HANS_TeamB_Synect_Phase1.1/Reports/TS_Phase1/20180626_184556/execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/TS_Phase1', suiteProperties, [new TestCaseBinding('Test Cases/Login/SDS_Open_Website', 'Test Cases/Login/SDS_Open_Website',  null), new TestCaseBinding('Test Cases/Login/SDS_Login_Invalid', 'Test Cases/Login/SDS_Login_Invalid',  null), new TestCaseBinding('Test Cases/Login/SDS_Login_Valid', 'Test Cases/Login/SDS_Login_Valid',  null), new TestCaseBinding('Test Cases/Register_User/Swagger_Open_Webpage', 'Test Cases/Register_User/Swagger_Open_Webpage',  null), new TestCaseBinding('Test Cases/Register_User/Swagger_Register_User', 'Test Cases/Register_User/Swagger_Register_User',  null), new TestCaseBinding('Test Cases/Register_User/Swagger_Register_Duplicate_User', 'Test Cases/Register_User/Swagger_Register_Duplicate_User',  null), new TestCaseBinding('Test Cases/Change_Password/SDMC_Open_Website', 'Test Cases/Change_Password/SDMC_Open_Website',  null), new TestCaseBinding('Test Cases/Change_Password/SDMC_Login', 'Test Cases/Change_Password/SDMC_Login',  null), new TestCaseBinding('Test Cases/Change_Password/SDMC_Reset_Password_Invalid_1', 'Test Cases/Change_Password/SDMC_Reset_Password_Invalid_1',  null), new TestCaseBinding('Test Cases/Change_Password/SDMC_Reset_Password_Invalid_2', 'Test Cases/Change_Password/SDMC_Reset_Password_Invalid_2',  null), new TestCaseBinding('Test Cases/Change_Password/SDMC_Reset_Password_Invalid_3', 'Test Cases/Change_Password/SDMC_Reset_Password_Invalid_3',  null), new TestCaseBinding('Test Cases/Change_Password/SDMC_Reset_Password_Valid', 'Test Cases/Change_Password/SDMC_Reset_Password_Valid',  null), new TestCaseBinding('Test Cases/Register_User/Swagger_Remove_User', 'Test Cases/Register_User/Swagger_Remove_User',  null)])
