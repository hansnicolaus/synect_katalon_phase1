<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>invalid_login_attempt</name>
   <tag></tag>
   <elementGuidId>08f93a79-dd83-4789-9e21-0381380535a3</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Invalid login attempt.</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loginForm&quot;)/form[@class=&quot;form-horizontal&quot;]/div[@class=&quot;validation-summary-errors text-danger&quot;]/ul[1]/li[1]</value>
   </webElementProperties>
</WebElementEntity>
