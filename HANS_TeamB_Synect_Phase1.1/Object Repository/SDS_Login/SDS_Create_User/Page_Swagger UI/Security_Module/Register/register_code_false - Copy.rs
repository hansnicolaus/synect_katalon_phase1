<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>register_code_false - Copy</name>
   <tag></tag>
   <elementGuidId>a64ee7b6-5183-4a36-a3ec-055bd3d6b08e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;SecurityModuleApi_SecurityModuleApi_Register_content&quot;)/div[@class=&quot;response&quot;]/div[@class=&quot;block response_body undefined&quot;]/pre[@class=&quot;json&quot;]/code[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>code</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>false
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;SecurityModuleApi_SecurityModuleApi_Register_content&quot;)/div[@class=&quot;response&quot;]/div[@class=&quot;block response_body undefined&quot;]/pre[@class=&quot;json&quot;]/code[1]</value>
   </webElementProperties>
</WebElementEntity>
