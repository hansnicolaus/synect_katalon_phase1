<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>register_code_200</name>
   <tag></tag>
   <elementGuidId>9fd7c185-8a7c-4696-a934-b2d27bfc7dd2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;SecurityModuleApi_SecurityModuleApi_Register_content&quot;)/div[@class=&quot;response&quot;]/div[@class=&quot;block response_code&quot;]/pre[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>pre</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>200</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;SecurityModuleApi_SecurityModuleApi_Register_content&quot;)/div[@class=&quot;response&quot;]/div[@class=&quot;block response_code&quot;]/pre[1]</value>
   </webElementProperties>
</WebElementEntity>
