<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_Phase1</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-06-27T11:05:02</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7ee9dc77-d634-422f-996a-627ed59b124e</testSuiteGuid>
   <testCaseLink>
      <guid>00c1dc4f-59e5-46d5-a2ff-045503bb693c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/SDS_Open_Website</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>308ddacc-d0f5-46be-b1d8-1d0b4b08d38e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/SDS_Login_Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c90a8672-b31f-4cab-b939-5b95801fd0ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/SDS_Login_Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>838199f1-aa2e-47ca-83f8-52c471ab75c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register_User/Swagger_Open_Webpage</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c3f64ba5-49aa-4c58-9747-da2c834de74b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register_User/Swagger_Register_User</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ec683476-46da-4540-865d-68b486278c48</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register_User/Swagger_Register_Duplicate_User</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5132ebf5-c997-46fa-b749-206399c728df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change_Password/SDMC_Open_Website</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>469e0b14-2b7d-40a8-9c04-923e50358d31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change_Password/SDMC_Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4fdff517-d7ac-4819-a5fb-568694b32414</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change_Password/SDMC_Reset_Password_Invalid_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b4035645-4dfb-4e36-947b-79d964a1a7c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change_Password/SDMC_Reset_Password_Invalid_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>14f26a0a-979f-47bb-ae11-1e11a6f5defc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change_Password/SDMC_Reset_Password_Invalid_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2dc40609-9bec-41d5-a51a-f9b45ddc3f82</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change_Password/SDMC_Reset_Password_Valid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb76347c-3eaa-4549-b4d1-70df0f4ef1c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register_User/Swagger_Remove_User</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
