//Hans Nicolaus
//Team B

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.After

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def Invalid_Login(username, password)
{	
	WebUI.setText(findTestObject('SDS_Login/SDS_Login_Page/input_Email'), username)
	WebUI.setText(findTestObject('SDS_Login/SDS_Login_Page/input_Password'), password)
	WebUI.click(findTestObject('SDS_Login/SDS_Login_Page/input_loginPage-button'))
}

def Username = 'Admin@synectmedia.com'
def Password = 'Admin@'

//WebUI.callTestCase(findTestCase('Test Cases/Login/SDS_Open_Website'), [:], FailureHandling.STOP_ON_FAILURE)

Invalid_Login(Username, Password)
assert WebUI.verifyElementPresent(findTestObject('SDS_Login/SDS_Login_Page/invalid_login_attempt'),10) : 'Error message not displayed' //account info must be present on top right
assert WebUI.verifyElementNotPresent(findTestObject('SDS_Login/SDS_Logged_In/loggout_form'),10) : 'User successfully logged in with invalid password'

Username = 'Admin@synect' //See notes!
Password = 'Admin@1234'

Invalid_Login(Username, Password)
assert WebUI.verifyElementPresent(findTestObject('SDS_Login/SDS_Login_Page/invalid_login_attempt'),10) : 'Error message not displayed' //account info must be present on top right
assert WebUI.verifyElementNotPresent(findTestObject('SDS_Login/SDS_Logged_In/loggout_form'),10) : 'User successfully logged in with invalid password'

//Notes: These invalid inputs will not display error message. UI issue.