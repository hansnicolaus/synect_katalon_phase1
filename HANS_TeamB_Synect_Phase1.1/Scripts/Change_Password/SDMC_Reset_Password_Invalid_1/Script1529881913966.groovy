//Hans Nicolaus
//Team B

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def Current_Password = 'ABC123'
def New_Password = 'KATALON1'
def Confirm_Password = 'KATALON1'

//WebUI.callTestCase(findTestCase('Test Cases/Change_Password/SDMC_Login'), [:], FailureHandling.STOP_ON_FAILURE) -> TS3_SDMC_Change_Password

WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_current_password'), Current_Password)
WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_new_password'), New_Password)
WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_confirm_password'), Confirm_Password)
WebUI.focus(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_confirm_password'))

assert WebUI.verifyElementNotPresent(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/button_Submit'),10,FailureHandling.CONTINUE_ON_FAILURE) : 'Submit button is not disabled'//See notes!

WebUI.click(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/button_Submit')) //See notes! Has to be clicked to trigger wrong password error message
assert WebUI.verifyElementPresent(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Errors/SDMC_wrong_username_password'),10,FailureHandling.CONTINUE_ON_FAILURE) : 'Error message not displayed'
assert WebUI.verifyElementNotPresent(findTestObject('SDMC_Change_Password/SDMC_Logged_In/div_logo'),10,FailureHandling.STOP_ON_FAILURE) :'User is logged in with incorrect login information'//STOP if user is logged in

WebUI.delay(5) //human delay

//Note: Submit button is not disabled. Submit button must be clicked to trigger error message. Requirement document/design issue; Submit button should be enabled even if user provided incorrect current password'