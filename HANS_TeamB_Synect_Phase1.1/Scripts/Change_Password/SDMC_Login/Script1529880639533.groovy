//Hans Nicolaus
//Team B

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.After

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def Username = 'KATALON'
def Current_Password = 'KATALON'

def User_Login(username, password)
{
	WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Login_Page/Elements/SDMC_input_username'), username)
	WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Login_Page/Elements/SDMC_input-password'), password)
	WebUI.click(findTestObject('SDMC_Change_Password/SDMC_Login_Page/Elements/SDMC_sign_in_button'))
}

//WebUI.callTestCase(findTestCase('Test Cases/Change_Password/SDMC_Open_Website'),[:], FailureHandling.STOP_ON_FAILURE) -> TS3_SDMC_Change_Password

User_Login(Username, Current_Password)

if(WebUI.verifyElementPresent(findTestObject('SDMC_Change_Password/SDMC_Login_Page/Errors/SDMC_invalid_login'),10,FailureHandling.OPTIONAL))
{
	//Fail to login assumes user is not yet created. Otherwise, error.
	WebUI.callTestCase(findTestCase('Test Cases/Register_User/Swagger_Open_Webpage'), [:], FailureHandling.STOP_ON_FAILURE)
	WebUI.callTestCase(findTestCase('Test Cases/Register_User/Swagger_Register_User'), [:], FailureHandling.STOP_ON_FAILURE)
	WebUI.callTestCase(findTestCase('Test Cases/Change_Password/SDMC_Open_Website'),[:], FailureHandling.STOP_ON_FAILURE)
	
	User_Login(Username, Current_Password)
}

assert WebUI.verifyElementPresent(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_change_password_message'),10) : 'User not redirected to SDMC change password webpage'

assert WebUI.verifyElementPresent(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_current_password'),10) &&\
WebUI.verifyElementPresent(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_new_password'),10) &&\
WebUI.verifyElementPresent(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_confirm_password'),10) : 'Change password webpage missing text fields'