//Hans Nicolaus
//Team B

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def Current_Password = 'KATALON'
def New_Password = 'KATALON1'
def Confirm_Password = 'KATALON1'

//WebUI.callTestCase(findTestCase('Test Cases/Change_Password/SDMC_Login'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_current_password'), Current_Password)
WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_new_password'), New_Password)
WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_confirm_password'), Confirm_Password)
WebUI.focus(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_new_password')) //See notes!

assert WebUI.verifyElementClickable(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/button_Submit'),FailureHandling.STOP_ON_FAILURE) : 'User unable to click Submit button'
WebUI.click(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/button_Submit'))
WebUI.delay(5)
assert WebUI.verifyElementClickable(findTestObject('SDMC_Change_Password/SDMC_Logged_In/div_KATALONLog out')) &&\
WebUI.verifyElementPresent(findTestObject('SDMC_Change_Password/SDMC_Logged_In/div_logo'),10,FailureHandling.STOP_ON_FAILURE) : 'User is not redirected to SDMC homepage' //verify user is logged in. STOP if user is not logged in, because further tests will not succeed.

//Note: Submit button will be disabled and previous error message will still be displayed even though Katalon Studio provided valid inputs.
//Solution: User has to click new password input field to remove previous error message and make Submit button clickable.
//Katalon Studio issue/design issue.