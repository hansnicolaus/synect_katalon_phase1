//Hans Nicolaus
//Team B

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import org.junit.After
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://sdmc-qa-cloud.azurewebsites.net/')
assert WebUI.getWindowTitle() == 'SDMC' : 'Failed to open SDMC login webpage.'
WebUI.delay(3)
assert WebUI.verifyElementPresent(findTestObject('SDMC_Change_Password/SDMC_Login_Page/Elements/SDMC_input_username'),1) &&\
WebUI.verifyElementPresent(findTestObject('SDMC_Change_Password/SDMC_Login_Page/Elements/SDMC_input-password'),1) &&\
WebUI.verifyElementPresent(findTestObject('SDMC_Change_Password/SDMC_Login_Page/Elements/SDMC_sign_in_button'),1) : 'SDMC login page missing elements'
//ASSERT if webpage opens

WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Login_Page/Elements/SDMC_input_username'), 'KATALON')
WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Login_Page/Elements/SDMC_input-password'), 'KATALON')
WebUI.click(findTestObject('SDMC_Change_Password/SDMC_Login_Page/Elements/SDMC_sign_in_button'))
assert WebUI.verifyElementPresent(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_change_password_message'),1)
assert WebUI.verifyElementPresent(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_current_password'),1) &&\
WebUI.verifyElementPresent(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_new_password'),1) &&\
WebUI.verifyElementPresent(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_confirm_password'),1)
assert WebUI.verifyElementNotPresent(findTestObject('SDMC_Change_Password/SDMC_Login_Page/Elements/SDMC_sign_in_button'),1)
//ASSERT if change password webpage opens

WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_current_password'), 'ABC123')
WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_new_password'), 'KATALON1')
WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_confirm_password'), 'KATALON1')
WebUI.focus(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_confirm_password'))
WebUI.click(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/button_Submit'))
assert WebUI.verifyElementPresent(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Errors/SDMC_wrong_username_password'),1)
WebUI.delay(1)
//ASSERT WebUI.click(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Errors/SDMC_wrong_username_password'))

WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_current_password'), 'KATALON')
WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_new_password'), 'KATAL')
WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_confirm_password'), 'KATAL')
WebUI.focus(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_confirm_password'))
assert WebUI.verifyElementPresent(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Errors/SDMC_error_password_six'),1)
assert WebUI.verifyElementNotPresent(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/button_Submit'),1)
WebUI.delay(1)

WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_current_password'), 'KATALON')
WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_new_password'), 'KATALON1')
WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_confirm_password'), 'KATALON2')
WebUI.focus(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_confirm_password'))
assert WebUI.verifyElementPresent(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Errors/p_Passwords must match'),1)
assert WebUI.verifyElementNotPresent(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/button_Submit'),1)
WebUI.delay(1)

//IDEAL SCENARIO!!!! -> FAILS
//WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_current_password'), 'KATALON')
//WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_new_password'), 'KATALON1')
//WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_confirm_password'), 'KATALON1')
//WebUI.waitForElementPresent(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/button_Submit'),3)
//assert WebUI.verifyElementClickable(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/button_Submit')) //UI ERROR: Button not automatically present when correct inputs are provided

WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_current_password'), 'KATALON')
WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_new_password'), 'KATALON1')
WebUI.setText(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_confirm_password'), 'KATALON1')
WebUI.focus(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/SDMC_input_new_password')) //!!!!!!! NEEDED TO FIX
assert WebUI.verifyElementClickable(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/button_Submit'))
WebUI.click(findTestObject('SDMC_Change_Password/SDMC_Change_Password_Webpage/Elements/button_Submit'))
assert WebUI.verifyElementClickable(findTestObject('SDMC_Change_Password/SDMC_Logged_In/div_KATALONLog out')) &&\
WebUI.verifyElementPresent(findTestObject('SDMC_Change_Password/SDMC_Logged_In/div_logo'),3)

WebUI.closeBrowser()

