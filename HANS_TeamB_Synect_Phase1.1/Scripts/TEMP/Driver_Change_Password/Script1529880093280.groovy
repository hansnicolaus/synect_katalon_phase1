 //Hans Nicolaus
//Team B
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Change_Password/SDMC_Open_Website'), [('url') : 'https://sdmc-qa-cloud.azurewebsites.net/'], 
    FailureHandling.STOP_ON_FAILURE //if user cannot open SDMC website, STOP because further testing will fail
    )

WebUI.callTestCase(findTestCase('Change_Password/SDMC_Login'), [('Username') : 'KATALON', ('Current_Password') : 'KATALON'], 
    FailureHandling.STOP_ON_FAILURE //if user is not redirected to change password page, STOP because further testing will fail
    )

WebUI.callTestCase(findTestCase('Change_Password/SDMC_Reset_Password_Invalid_1'), [('Current_Password') : 'ABC123', ('New_Password') : 'KATALON1'
        , ('Confirm_Password') : 'KATALON1'], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('Change_Password/SDMC_Reset_Password_Invalid_2'), [('Current_Password') : 'KATALON', ('New_Password') : 'KATAL'
        , ('Confirm_Password') : 'KATAL'], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('Change_Password/SDMC_Reset_Password_Invalid_3'), [('Current_Password') : 'KATALON', ('New_Password') : 'KATALON1'
        , ('Confirm_Password') : 'KATALON2'], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('Change_Password/SDMC_Reset_Password_Valid'), [('Current_Password') : 'KATALON', ('New_Password') : 'KATALON1'
        , ('Confirm_Password') : 'KATALON1'], FailureHandling.STOP_ON_FAILURE //if user is not logged in, STOP because further testing will fail
    )

