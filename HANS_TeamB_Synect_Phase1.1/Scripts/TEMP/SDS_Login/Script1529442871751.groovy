//Hans Nicolaus
//Team B

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//SDS_Login Test Case Starts
WebUI.openBrowser('')

WebUI.navigateToUrl('http://sds-qa-cloud-1.azurewebsites.net/')
assert WebUI.getWindowTitle() == 'Login' : 'Fail to reach SDS website' //browser window title must be "Login"
//must contain all elements of SDS login page
assert WebUI.verifyElementPresent(findTestObject('SDS_Login/SDS_Login_Page/img_loginPage-logo'),3) &&\
WebUI.verifyElementPresent(findTestObject('SDS_Login/SDS_Login_Page/input_Email'),3) &&\
WebUI.verifyElementPresent(findTestObject('SDS_Login/SDS_Login_Page/input_Password'),3) &&\
WebUI.verifyElementPresent(findTestObject('SDS_Login/SDS_Login_Page/input_loginPage-button'),3) : 'SDS login page missing login elements'

WebUI.setText(findTestObject('SDS_Login/SDS_Login_Page/input_Email'), 'Admin@synectmedia.com')
WebUI.setText(findTestObject('SDS_Login/SDS_Login_Page/input_Password'), '1234')
WebUI.click(findTestObject('SDS_Login/SDS_Login_Page/input_loginPage-button'))
assert WebUI.verifyElementPresent(findTestObject('SDS_Login/SDS_Login_Page/invalid_login_attempt'),3) : 'Fail to login to SDS website as Admin' //account info must be present on top right

WebUI.setText(findTestObject('SDS_Login/SDS_Login_Page/input_Email'), 'Admin@synectmedia.com')
WebUI.setText(findTestObject('SDS_Login/SDS_Login_Page/input_Password'), 'Admin@1234')
WebUI.click(findTestObject('SDS_Login/SDS_Login_Page/input_loginPage-button'))
assert WebUI.verifyElementPresent(findTestObject('SDS_Login/SDS_Logged_In/loggout_form'),3) : 'Fail to login to SDS website as Admin' //account info must be present on top right
//SDS_Login Test Case Ends

//SDS_Create_User Test Case Starts
WebUI.click(findTestObject('SDS_Login/SDS_Create_User/SDS_Logged_In/a_Dev Tools'))
WebUI.click(findTestObject('SDS_Login/SDS_Create_User/SDS_Logged_In/a_Swagger API'))
assert WebUI.verifyElementPresent(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/a_swagger'),3) : 'Admin is not redirected to Swagger UI' //Admin must be in Swagger UI


WebUI.waitForElementVisible(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/SecurityModuleApi'),2)
WebUI.click(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/SecurityModuleApi'))

WebUI.waitForElementVisible(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/SecurityModule_Register'),2)
WebUI.click(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/SecurityModule_Register'))

WebUI.waitForElementVisible(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_textarea'),2)
WebUI.setText(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_textarea'), '{"Username": "KATALON", "Password": "KATALON", "Token": "string"}')
WebUI.waitForElementVisible(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_input_submit'),2)
WebUI.click(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_input_submit'))

if(WebUI.getText(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_code_true')) == 'false')
{
	//test to see if 'false' is caused by an account with same username and password is already registered
	//by trying to delete the user
	//failure to delete means failure to register a new user with unique username and password
	WebUI.click(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/RemoveUser/SecurityModule_RemoveUser'))
	
	WebUI.waitForElementVisible(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/RemoveUser/removeuser_textarea (1)'),2)
	WebUI.setText(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/RemoveUser/removeuser_textarea (1)'), '{"Username": "KATALON", "Password": "KATALON", "Token": "string"}')
	WebUI.waitForElementVisible(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/RemoveUser/removeuser_input_submit'),2)
	WebUI.click(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/RemoveUser/removeuser_input_submit'))
	assert WebUI.verifyElementText(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/RemoveUser/removeuser_code_true'),'true') : 'Fail to register a new user account with unique username and password.'
	
	//WebUI.click(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/RemoveUser/SecurityModule_RemoveUser'))
	
	WebUI.waitForElementVisible(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_textarea'),2)
	WebUI.setText(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_textarea'), '{"Username": "KATALON", "Password": "KATALON", "Token": "string"}')
	WebUI.waitForElementVisible(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_input_submit'),2)
	WebUI.click(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_input_submit'))
}
WebUI.delay(1)
assert WebUI.verifyElementText(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_code_200'),'200') : 'Server is unavailable'
assert WebUI.verifyElementText(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_code_true'),'true') : 'Fail to register user'

WebUI.waitForElementVisible(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_input_submit'),2)
WebUI.click(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_input_submit'))
WebUI.delay(1)
assert WebUI.verifyElementText(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_code_200'),'200') : 'Server is unavailable'
assert WebUI.verifyElementText(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_code_true'),'false') : 'Duplicate user account created'
//SDS_Create_User Test Case Ends

WebUI.closeBrowser()
