//Hans Nicolaus
//Team B

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.After

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//WebUI.callTestCase(findTestCase('Test Cases/Register_User/Swagger_Open_Webpage'), [:], FailureHandling.STOP_ON_FAILURE) -> TS_Register_User

def Username = 'KATALON'
def Password = 'KATALON'

def register_user(username, password)
{
	WebUI.waitForElementVisible(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_textarea'),10)
	def input = '{"Username": "' + username + '", "Password": "' + password + '", "Token": "string"}'
	WebUI.setText(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_textarea'), input)
	WebUI.waitForElementVisible(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_input_submit'),10)
	WebUI.click(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_input_submit'))
}

def remove_user(username, password)
{
	WebUI.waitForElementVisible(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/RemoveUser/removeuser_textarea (1)'),10)
	def input = '{"Username": "' + username + '", "Password": "' + password + '", "Token": "string"}'
	WebUI.setText(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/RemoveUser/removeuser_textarea (1)'), input)
	WebUI.waitForElementVisible(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/RemoveUser/removeuser_input_submit'),10)
	WebUI.click(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/RemoveUser/removeuser_input_submit'))
}

WebUI.waitForElementVisible(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/SecurityModuleApi'),10)
WebUI.click(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/SecurityModuleApi'))

WebUI.waitForElementVisible(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/SecurityModule_Register'),10)
WebUI.click(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/SecurityModule_Register'))

register_user(Username, Password)
WebUI.delay(5)
if(WebUI.getText(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_code_true')) == 'false')
{
	//test to see if 'false' is caused by an account with same username and password is already registered
	//by trying to delete the user
	//failure to delete means failure to register a new user with unique username and password
	WebUI.click(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/RemoveUser/SecurityModule_RemoveUser'))
	WebUI.delay(5)
	remove_user(Username, Password)
	assert WebUI.verifyElementText(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/RemoveUser/removeuser_code_true'),'true') : 'Fail to register a new user account with unique username and password.'
		
	register_user(Username, Password)
}

WebUI.delay(5)

assert WebUI.verifyElementText(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_code_200'),'200') : 'Server is unavailable'
assert WebUI.verifyElementText(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_code_true'),'true') : 'Fail to register user'