//Hans Nicolaus
//Team B

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//WebUI.callTestCase(findTestCase('Test Cases/Register_User/Swagger_Register_User'),[:]) -> TS_Register_User

WebUI.waitForElementVisible(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_input_submit'),5)
WebUI.click(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_input_submit'))
WebUI.delay(3)
assert WebUI.verifyElementText(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_code_200'),'200') : 'Server is unavailable'
assert WebUI.verifyElementText(findTestObject('SDS_Login/SDS_Create_User/Page_Swagger UI/Security_Module/Register/register_code_true'),'false') : 'System allows duplicate user to be created.'